import cv2
from threading import Thread
from flask import Flask, render_template, Response, jsonify

import realsense_camera as cam
# import stm_communication as stm
import state_machine as sm


flag = False

app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')



def gen(camera):
    while True:
        # frame = sm.camera.get_frame()
        if sm.obst:
            # frame = sm.camera.color_frame
            ret, jpeg = cv2.imencode('.jpg', sm.camera.color_frame, [int(cv2.IMWRITE_JPEG_QUALITY), 50])
            frame = jpeg.tobytes()
            yield (b'--frame\r\n'
                b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')



@app.route('/video_feed')
def video_feed():
    return Response(gen(sm.camera),
                    mimetype='multipart/x-mixed-replace; boundary=frame')


@app.route('/check')
def check():
    if sm.obst:
        return jsonify({'status': sm.obst_type})
    else:
        return jsonify({'status': "None"})  



@app.route('/swich', methods=['POST'])
def swich():
    global flag
    flag = True if flag == False else False
    print(flag)
    return jsonify({'status': True})



if __name__ == "__main__":
    try:
        state_machine_thread = Thread(target=sm.state_machine, \
                    name = "state_machine", daemon = True)
        state_machine_thread.start()

        app.run(host='0.0.0.0', debug=False)
    finally:
        state_machine_thread.join()
