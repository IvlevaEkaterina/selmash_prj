from threading import Thread
from time import sleep

import realsense_camera as cam
import stm_communication as stm



'''
robot states can be:
find_line, line, end_line, turn, detour
'''



def obstacle_thread():
    global obst, obst_dist, obst_type
    while camera.enable:
        if state == "line":
            obst, obst_dist = camera.findObstacle()
            if obst:
                obst_type = camera.getObstackleType()
                print("obst is ", obst, "  dist = ", obst_dist, \
                        "  obst_type = ", obst_type)
            else:
                obst = False
        else:
            obst = False
        sleep(0.01)


camera = cam.RealSenseCamera()



def state_machine():
    global state
    state = 'find_line'
    prev_state = 'find_line'
    line_num = 2
    line_cnt = 0
    reverse_line = 1
    direction = 1 # 1 - left, -1 - right
    detour_dir = -1
    global obst, obst_dist, obst_type
    obst = False

    # camera = cam.RealSenseCamera()
    uart = stm.UART()
    uart.uartSetup(115200)

    thread = Thread(target=obstacle_thread, daemon=True)
    thread.start()

    try:
        while True:
            # frame = camera.getVideoFrame()
            # cv2.imshow("frame", camera.color_frame)
            # print("dir = ", direction)
            if state == 'find_line':
                print("state find_line")
                pckt = uart.preparePocket(0x02, 0, 0, 0)
                isLine, line_list = camera.findLine()
                dev_angle = camera.getDeviationFromLine(line_list, 5)

                if dev_angle != None:
                    state = 'line'
                    if prev_state != 'detour':
                        line_cnt += 1
                    prev_state = 'find_line'
                    print('LINE WAS FOUND')

            elif state == 'line': # 0x02
                if obst == True and obst_dist < 0.59:
                    if obst_type == "TREE" or \
                       obst_type == "PYLON":
                        if line_cnt == 1:
                            detour_dir = direction * (-1)
                        else:
                            detour_dir = direction
                        speed = 0.2
                        radius = 0.6
                        pckt = uart.preparePocket(0x04, detour_dir, speed, radius)
                        state = "detour"
                        prev_state = 'line'
                    else:
                        pckt = uart.preparePocket(0x02, 0, 0, 0)
                    for i in range(5):
                        uart.transmitData(pckt)
                else:
                    print("state line")
                    isLine, line_list = camera.findLine()
                    dev_angle = camera.getDeviationFromLine(line_list, 5)

                    if dev_angle is not None:
                        distance = 0
                        pckt = uart.preparePocket(0x02, dev_angle, 0.2, distance)
                        uart.transmitData(pckt)
                        print("LINE")
                    else:
                        dev_angle = 0
                        distance = 0.6
                        pckt = uart.preparePocket(0x02, dev_angle, 0.2, distance)
                        for i in range(5):
#                        print("transmit data that line is lost")
#                        print("distance = ", distance)
                            uart.transmitData(pckt)
                        state = 'end_line'
                        prev_state = 'line'
                        print('CAMERA LOST THE LINE')

#                pckt = uart.preparePocket(0x02, dev_angle, 0.2, distance)
#                uart.transmitData(pckt)

#                print(dev_angle)

            elif state == 'end_line': # 0x02
                print("state end_line")
                inMessage = uart.reservData()
#                print("message length is ", len(inMessage))
#                print("message = ", inMessage, "\n")
                if uart.parseData(inMessage) == 1:
                    print("END OF THE LINE")
                    state = 'turn'
                    prev_state = 'end_line'

                    if line_cnt == line_num:
                        if line_num % 2 == 0:
                            reverse_line = 1
                        else:
                            reverse_line = reverse_line * (-1)
                        line_cnt = 1

                    if line_cnt % 2 == 0:
                        direction = -1
                    else:
                        direction = 1

                    direction = direction * reverse_line
#                     print("line_cnt = ", line_cnt, "  direction = ", direction) 
                    pckt = uart.preparePocket(0x03, direction, 0.2, 0.56)
                    for i in range(5):
#                        print("transmit data that turn is done")
                        uart.transmitData(pckt)
#                print('end_line')

            elif state == 'turn':   # state = 'turn' = 0x03
                print("state turn")
                inMessage = uart.reservData()
                if uart.parseData(inMessage) == 2:
                    print("TURN IS DONE")
                    state = 'find_line'

            elif state == 'detour':     # state = 'detour' = 0x04
                print("state detour")
                inMessage = uart.reservData()
                if uart.parseData(inMessage) == 3:
                    print("DETOUR IS DONE")
                    state = 'find_line'
                    prev_state = 'detour'

            sleep(0.001)

    finally:
        print('Programm closing')
        thread.join()
        pckt = uart.preparePocket(0x02, 0, 0, 0)
        for i in range(5):
            uart.transmitData(pckt)

        try:
            camera.pipeline.stop()
            print('Camera was released')
        except:
            print('Camera stil busy')
        print('Programm closed')



if __name__ == "__main__":
    state_machine()
