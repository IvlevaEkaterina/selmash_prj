import pyrealsense2 as rs2
import numpy as np
import cv2
import math
from threading import Thread

from time import sleep
from time import time



class RealSenseCamera:
    def __init__(self):
        self.enable = False
        self.cameraSetup()
        self.getYellowColorRange()


    def cameraSetup(self):
        try:
            self.pipeline.stop()
            print('Camera stoped')
        except:
            try:
                self.pipeline = rs2.pipeline() # Объект pipeline содержит методы для взаимодействия с потоком
                config = rs2.config() # Дополнительный объект для хранения настроек потока
                config.enable_stream(rs2.stream.color, 640, 480, rs2.format.bgr8, 15)
                config.enable_stream(rs2.stream.depth, 640, 480, rs2.format.z16, 6)
                self.profile = self.pipeline.start(config)

                self.enable = True
            except Exception as err:
                print(f'Ошибка камеры: {err}')


    def getYellowColorRange(self):
        self.light_yellow = np.array([0, 0, 80], dtype=np.uint8)
        self.dark_yellow = np.array([85, 200, 255], dtype=np.uint8)
 


    def getVideoFrame(self):
        frames = self.pipeline.wait_for_frames()
        c_frame = frames.get_color_frame()
        if not c_frame:
            print("There is no frame")
            return None
        else:
            self.color_frame = np.asanyarray(c_frame.get_data())
            return self.color_frame



    def get_frame(self):
        try:
            frames = self.pipeline.wait_for_frames()
            c_frame = frames.get_color_frame()
    
            self.color_frame = np.asanyarray(c_frame.get_data())
           # cv2.imshow("frame", self.color_frame)
            
        except:
            self.color_frame = np.zeros((480,640,3),np.uint8)

        ret, jpeg = cv2.imencode('.jpg', self.color_frame, [int(cv2.IMWRITE_JPEG_QUALITY), 50])

        return jpeg.tobytes()


    



    ''' OPENCV CAMERA '''
    def cameraSetupCV(self):
        self.camera = cv2.VideoCapture(4)
        return self.camera.isOpened()


    def getVideoFrameCV(self):
        if self.camera.isOpened():
            _, self.color_frame = self.camera.read()
            return self.color_frame
        else:
            return None


    def closeCameraCV(self):
        self.camera.release()
    ''' ----------- '''



    def findLine(self):
        coeff = 0.5
        small_frame = cv2.resize(self.color_frame, None, fx=coeff, fy=coeff) 
        # cv2.imshow("small_frame", small_frame)
        # размытие кадра и перевод преобразование BGR -> HSV
        median_blur = cv2.medianBlur(small_frame, 9)
        hsv_median_blur = cv2.cvtColor(median_blur, cv2.COLOR_BGR2HSV)
        # создание маски для поиска желтого цвета на кадрах
        mask_yellow = cv2.inRange(hsv_median_blur, self.light_yellow, self.dark_yellow)
        # маски для фильтрации шумов
        kernel = np.ones((7,7), np.uint8)
        erosion_mask = cv2.erode(mask_yellow, kernel, iterations = 1)
        morphology_mask = cv2.morphologyEx(erosion_mask, cv2.MORPH_OPEN, kernel)
        dilate_mask = cv2.dilate(morphology_mask, kernel, iterations = 1)
        # дробим маску на 4 части по оси y
        h, w = dilate_mask.shape
        roi = [dilate_mask[0                : h // 4,       0 : w], \
               dilate_mask[h // 4 + 1       : h // 2,       0 : w], \
               dilate_mask[h // 2 + 1       : (h * 3) // 4, 0 : w], \
               dilate_mask[(h * 3) // 4 + 1 : h,            0 : w]]
        # отрисовка линий на выводимом на экран изображении с камеры
        # cv2.line(frame, (0, int((h // 4) / coeff)), \
        #         (int(w / coeff), int((h // 4) / coeff)), (255,0,0), 1)
        # cv2.line(frame, (0, int((h // 2) / coeff)), \
        #         (int(w / coeff), int((h // 2) / coeff)), (255,0,0), 1)
        # cv2.line(frame, (0, int(((h * 3) // 4) / coeff)), \
        #         (int(w / coeff), int(((h * 3) // 4) / coeff)), (255,0,0), 1)
        # cv2.circle(frame, (320, 480), 5, (255, 0, 255), -1)
        # поиск контуров линии на каждом участке изображения отдельно
        # и определение их центров
        line = [] # [[line_contour, line_contour_area, np.array([cX, cY])]]
        for i, r in enumerate(roi):
            _, contours, _ = cv2.findContours(r.copy(), cv2.RETR_EXTERNAL, \
                                    cv2.CHAIN_APPROX_SIMPLE) # RETR_TREE
            if contours is not None and len(contours) > 0:
                line_contour = max(contours, key = cv2.contourArea)
                line_contour_area = cv2.contourArea(line_contour)
                # print('contour area = ', line_contour_area)
                if line_contour_area >= 2000:
                    M = cv2.moments(line_contour)
                    cX = int(M["m10"] / (M["m00"] + 1e-7))
                    cY = int(M["m01"] / (M["m00"] + 1e-7)) + (h // 4) * i
                    line.append([line_contour, line_contour_area, cX, cY])
                    # отрисовка центров фрагментов линии
                    # cv2.circle(frame, (int(cX / coeff), int(cY / coeff)), \
                    #                     5, (255, 255, 255), -1)
                else:
                    line.append([])
            else:
                line.append([])

        return (len(line[0]) != 0 or len(line[1]) != 0 or \
                len(line[2]) != 0 or len(line[3]) != 0), line



# 640 x 480
    def getDeviationFromLine(self, line_list, filter):
        if len(line_list[1]) != 0:
#            print("lx = ", line_list[1][2], "ly = ", line_list[1][3])
            dx = line_list[1][2] - 160
            dy = line_list[1][3] - 240
#            print('dx = ',  dx, " dy = ", dy)
            dev_angle = math.atan(dx / dy)
            return dev_angle
        else:
            return None


    def findObstacle(self):   # findObstacle
        depth_sensor = self.profile.get_device().first_depth_sensor()
        depth_scale = depth_sensor.get_depth_scale()
        # print("Depth Scale is: " , depth_scale)

        # We will be removing the background of objects more than
        # clipping_distance_in_meters meters away
        clipping_distance_in_meters = 0.63 # 1 meter
        clipping_distance = clipping_distance_in_meters / depth_scale

        # Create an align object
        # rs.align allows us to perform alignment of depth frames to others frames
        # The "align_to" is the stream type to which we plan to align depth frames.
        align_to = rs2.stream.color
        align = rs2.align(align_to)

        depth_frames = self.pipeline.wait_for_frames()

        # Align the depth frame to color frame
        aligned_depth_frames = align.process(depth_frames)
        # Get aligned depth_frames
        aligned_depth_frame = aligned_depth_frames.get_depth_frame() # aligned_depth_frame is a 640x480 depth image
        
        if not aligned_depth_frame:
            print("no depth frame")
            return None
        else:
            depth_frame = np.asanyarray(aligned_depth_frame.get_data())

            # Remove background - Set pixels further than clipping_distance to grey
            black_color = 0 # 153
            depth_image_3d = np.dstack((depth_frame,depth_frame,depth_frame)) #depth image is 1 channel, color is 3 channels
            try:
                bg_removed = np.where((depth_image_3d > clipping_distance) | (depth_image_3d <= 0), \
                                    black_color, self.color_frame)
            except:
                print("No color frame")
            # Render images
            depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(depth_frame, alpha=0.03), cv2.COLORMAP_JET)
            # images = np.hstack((bg_removed, depth_colormap))
            # cv2.imshow("smth", bg_removed)
            # colorizer = rs2.colorizer() # <- Пригодится для отрисовки цветной карты глубины
            # colorized_depth = cv2.applyColorMap(cv2.convertScaleAbs(depth_frame, alpha=0.03), cv2.COLORMAP_JET)


            ''' ПОИСК ПРЕПЯТСТВИЯ '''
            roi = bg_removed[0 : 480 // 4, 50 : 640 - 50].copy()
            median_roi = cv2.medianBlur(roi, 9)
            grey_roi = cv2.cvtColor(median_roi, cv2.COLOR_BGR2GRAY)
            threshold_roi = cv2.adaptiveThreshold(grey_roi, 255, \
                            cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 11, 2)
            threshold_roi = 255 - threshold_roi # invert threshold image

            _, contours, hierarchy = cv2.findContours(threshold_roi.copy(), \
                    cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE) # RETR_TREE
            # cv2.drawContours(roi, contours, -1, (0, 0, 255), 3)
            # cnts = imutils.grab_contours(contours)
            # roiymp = roi.copy()
            # cv2.drawContours(roiymp, cnts, -1, (0, 0, 255), 3)
            # cv2.imshow('roiymp', roiymp)
            isObstacle = False 
            obstacle_dist = 0
            if contours is not None and len(contours) > 0:
                isObstacle = True
                obstacle_contour = max(contours, key = cv2.contourArea)
                M = cv2.moments(obstacle_contour)
                cX = int(M["m10"] / (M["m00"] + 1e-7)) + 50
                cY = int(M["m01"] / (M["m00"] + 1e-7))
#                cv2.circle(bg_removed, (cX, cY), 7, (255, 255, 255), -1)
                obstacle_dist = aligned_depth_frame.get_distance(cX, cY)
                print(obstacle_dist)
                x, y, w, h = cv2.boundingRect(obstacle_contour)
                # cv2.rectangle(roi, (x, y), (x+w, y+h), (0, 255, 0), 2)
                self.obst_roi = roi[y : y + h, x : x + w].copy()
#                cv2.imshow("OBST_ROI", self.obst_roi)
 #           cv2.imshow("ROI", roi)
            # cv2.imshow("th5", th5)

            return isObstacle, obstacle_dist



    def getObstackleType(self):
        # hsv_roi = cv2.cvtColor(self.obst_roi, cv2.COLOR_BGR2HSV)
        # cv2.imshow("hsv_roi", hsv_roi)
        # calculate the average color of each row of our image
        avg_color_per_row = np.average(self.obst_roi, axis=0)

        # calculate the averages of our rows
        avg_colors = np.average(avg_color_per_row, axis=0)
        
        # avg_color is a tuple in BGR order of the average colors
        # but as float values
#        print(f'avg_colors: {avg_colors}')

        # so, convert that array to integers
        int_averages = np.array(avg_colors, dtype=np.uint8)
        print(f'int_averages: {int_averages}')

        # hsv_averages = clr.rgb_to_hsv(int_averages[2], int_averages[1], int_averages[0])
        # print(f'hsv_averages: {hsv_averages}')
        
        h, w, _ = np.shape(self.obst_roi) 

        # create a new image of the same height/width as the original
        average_image = np.zeros((h, w, 3), np.uint8)
        # and fill its pixels with our average color
        average_image[:] = int_averages
        # hsv_average_img = cv2.cvtColor(average_image, cv2.COLOR_BGR2HSV)
#        cv2.imshow("hsv_clr", hsv_average_img)

        # finally, show it side-by-side with the original
#        cv2.imshow("Avg Color", np.hstack([self.obst_roi, average_image]))

        # dominant_color = np.array(self.obst_roi.histogram(), dtype=np.int32)
        # cv2.imshow('dc', dominant_color)

        max_clr = max(avg_colors)
        tmp_avg_clrs = avg_colors / max_clr
        clrs_sum = np.sum(avg_colors)
        avg_colors_list = avg_colors.tolist()
        max_clr_index = avg_colors_list.index(max_clr) 
#        print("index = ", max_clr_index)

        if clrs_sum > 200:
            return "COW"
        elif max_clr_index == 0: # blue
            return "HUMAN"
        elif max_clr_index == 1: # green
            return "TREE"
        else: # red
            return "PYLON"



    

    def getBgRemovedFrame(self):
        print(" ")


def test_thread():
    while camera.enable:
        obst, obst_dist = camera.findObstacle()
#         print("obst is ", obst, "\ndist = ", obst_dist)
        if obst:
            obst_type = camera.getObstackleType()
            print("obst is ", obst, "  dist = ", obst_dist, \
                    "  obst_type = ", obst_type)
        else:
            print("No obstacle")





if __name__ == "__main__":
    camera = RealSenseCamera()
    thread = Thread(target=test_thread, daemon=True)
    thread.start()
    try:
        while True:
            frame = camera.getVideoFrame()
#            cv2.imshow("frame", camera.color_frame)
#             obst, obst_dist = camera.getPointClouds(frame)
#             print("obst is ", obst, "\ndist = ", obst_dist)
            
            key = cv2.waitKey(10)
            if key == ord('d'):
                print('d')
#                 obst, obst_dist = camera.getPointClouds(frame)
#                 print("obst is ", obst, "\ndist = ", obst_dist)
#                 cv2.imshow('colorized depth frame', colorized_depth_frame)
#                 cv2.imshow('background removed', bg_removed)
            elif key == ord('q'):
                # camera.release()
                camera.enable = False
                cv2.destroyAllWindows()
                break
            cv2.imshow('frame', camera.color_frame)
    finally:
        print('Programm closing')
        thread.join()
        try:
            camera.pipeline.stop()
            print('Camera was released')
        except:
            print('Camera stil busy')
        print('Programm closed')
