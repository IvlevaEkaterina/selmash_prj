import struct
try:
    import serial
except:
    print("Error: Not raspberry or no ports")



class UART():
    ''' _UART_ '''

    def uartSetup(self, baudrate):
        ''' Настройка UART порта '''
        try:
            self.port = serial.Serial('/dev/ttyACM0', baudrate, timeout=None)
            print('STM connected to ttyACM0')
        except:
            try:
                self.port=serial.Serial('/dev/ttyACM1', baudrate, timeout=None)
                print('STM connected to ttyACM1')
            except:
                try:
                    self.port=serial.Serial('/dev/ttyACM2', baudrate, timeout=None)
                    print('STM connected to ttyACM2')
                except:
                    print('Error: Serial Denide')


    def transmitData(self, data):
        try:
            for d in data:
                self.port.write(d)
        except:
            pass


    def reservData(self):
        self.port.flush()
        data = self.port.read(8)

        return data



    def parseData(self, data):
        if len(data) == 8:
#            print("input msg = ", data[3])
            if data[0] == 0xFA and data[1] == 0xFA:
                return data[3]
        return None


    def preparePocket(self, state, data0, data1, data2):
        transmitedData = []
        transmitedData += [0xFA.to_bytes(1, 'little'),
                            0xAF.to_bytes(1, 'little'),
                            0x20.to_bytes(1, 'little'),
                            state.to_bytes(1, 'little')]
        data0 = struct.pack('f', data0)
        data1 = struct.pack('f', data1)
        data2 = struct.pack('f', data2)

        transmitedData += [data0, data1, data2]

        return transmitedData 
    
